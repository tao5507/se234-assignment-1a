package corejava.day10;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Bullet {
    public final static int UP = 0;
    private int x, y;//子弹中心坐标
    private int speed;
    private JPanel panel;
    private static Image fire=new ImageIcon("pic/fire.gif").getImage();
    public Bullet(int x, int y, int orientation) {
        this.x = x;
        this.y = y;
    }
    public Bullet(){}
    public void draw(Graphics g) {
        g.drawImage(fire, x-30, y-60, 60,60, panel);
    }
    public int getX() {return x;}
    public void setX(int x) {this.x = x;}
    public int getY() {return y;}
    public void setY(int y) {this.y = y;}
    public JPanel getPanel() {return panel;}
    public void setPanel(JPanel panel) {this.panel = panel;}
    public void move(){
        y--;
    }
    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
