package corejava.day10;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.URL;
import java.util.Random;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class FighterPanel extends JPanel implements MouseMotionListener,MouseListener,KeyListener{
	private static final long serialVersionUID = 1L;
	private int x,y;//飞机中心坐标
	private boolean isDead=false;//看自己的机器碰到敌机没
	private boolean isRestart=false;
	private static int totalCount=0;
	private Vector<Bullet> bullets;
	private Vector<Enemy> enemies;
	private boolean isFire=false;//是否为开火状态
	private static Image plane=new ImageIcon("pic/Mario.png").getImage();
	private static Image backImage=new ImageIcon("pic/Background.png").getImage();
	
	FighterPanel(){
		x=180;
		y=500;
		bullets=new Vector<Bullet>();
		enemies=new Vector<Enemy>();
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.addKeyListener(this);
		Toolkit tk=Toolkit.getDefaultToolkit();
		URL classUrl=this.getClass().getResource("");
		Image cursorImage=tk.getImage(classUrl);
		Cursor cs=tk.createCustomCursor(cursorImage,new Point(0,0),"selfCursor");
		setCursor(cs);
	}
	public void paint(Graphics g){
		//super.paint(g);
		int imgNum=0;
		//先画背景
		setBackground(Color.BLACK);
		g.drawImage(backImage, 0, 0, 600, 700, this);
		//先画敌机
		for(int i=0;i<enemies.size();i++){
			Enemy en=enemies.elementAt(i);
			en.draw(g);
		}
		//显示得分
		g.setColor(Color.GREEN);
		g.setFont(new Font("宋体",Font.BOLD,20));
		g.drawString("得分"+totalCount, 500, 20);
		//GAME OVER
		if(isDead){
			g.setColor(Color.RED);
			g.setFont(new Font("宋体",Font.BOLD,50));
			g.drawString("GAME OVER!\n\r",150,250);
		}
		//再画飞机（自己的飞机就飞得更高）
		g.drawImage(plane, x-40, y-40, 80, 80, this);
		//最后画子弹
		for(int i=0;i<bullets.size();i++){
			Bullet ball=bullets.elementAt(i);
			ball.draw(g);
		}
		//如果敌机爆炸
		for(int i=0;i<enemies.size();i++){
			Enemy enm=enemies.elementAt(i);
			if(enm.getIsBombed()==true){
				System.out.println("isBombed=true");
				g.drawImage(Enemy.bomb[imgNum],x,y,80,80, this);
				try {Thread.sleep(200);}
				catch (InterruptedException e) {}
				
				//g.drawImage(bomb[1],x,y,80,80, panel);
				//g.drawImage(bomb[2],x,y,80,80, panel);
				//g.drawImage(bomb[3],x,y,80,80, panel);
			}
		}
		
	}
	public void mouseDragged(MouseEvent e) {
		x=e.getX();
		y=e.getY();
		repaint();//重画此组件
	}
	public void mouseMoved(MouseEvent e) {
		x=e.getX();
		y=e.getY();	
		repaint();
	}
	public void mouseClicked(MouseEvent e) {
		
	}//鼠标点击
	public void mouseEntered(MouseEvent e) {}//鼠标到图形组件中
	public void mouseExited(MouseEvent e) {}//鼠标移出组件
	public void mousePressed(MouseEvent e) {//压下
		isFire=true;
	}
	public void mouseReleased(MouseEvent e) {//释放
		isFire=false;
	}
	public void startRun(){
		new Thread(){
			public void run(){
				int countTime=0;
				Random random =  new Random();
				while(true){
					if(countTime%250==0){//每隔一段时间添加一个敌机
						Enemy eny=new Enemy();
						eny.setOrientation(random.nextInt(100)%7);
						eny.setY(-40);//从顶端出现
						eny.setX(random.nextInt(1000)-200);//宽度随机
						eny.setIsBombed(false);//开始时没有爆炸
						enemies.add(eny);
					}
					//要敌机移动
					if(countTime%10==0){
						for(int i=0;i<enemies.size();i++){
							Enemy en=enemies.elementAt(i);
							en.move();
						}
						//删除越界的飞机
						for(int i=0;i<enemies.size();i++){
							Enemy en=enemies.elementAt(i);
							if(en.getX()<=-40||en.getX()>=640||en.getY()>=640){
								enemies.remove(i);
							}
						}
					}
					//如果是开火状态，则添加子弹
					if(isFire&&countTime%80==0){
						Bullet ball=new Bullet();
						ball.setX(x);
						ball.setY(y);
						bullets.add(ball);//将球的对象添加到集合中
					}
					//判断球是否越界，若是则删除
					for(int i=0;i<bullets.size();i++){
						Bullet ball=bullets.elementAt(i);
						if(ball.getY()<=0){
							bullets.remove(i);//删除数组中的第i个球
						}
					}
					//让子弹移动
					for(int i=0;i<bullets.size();i++){
						Bullet ball=bullets.elementAt(i);
						ball.move();
					}
					//看子弹是否打中敌机
					for(int i=0;i<bullets.size();i++){
						int buX=bullets.elementAt(i).getX();
						int buY=bullets.elementAt(i).getY();
						for(int j=0;j<enemies.size();j++){
							int enX=enemies.elementAt(j).getX();
							int enY=enemies.elementAt(j).getY();
							int length=(int)(Math.sqrt((buX-enX)*(buX-enX)+(buY-enY)*(buY-enY)));
							if(length<60){//如果子弹和敌机的距离相距在60个像素以内，就认为是打到了
								enemies.elementAt(j).setIsBombed(true);
								totalCount++;
								repaint();
								bullets.remove(i);
								enemies.remove(j);
							}
						}
					}
					//看敌机是否撞到自己
					for(int i=0;i<enemies.size();i++){
						int enX=enemies.elementAt(i).getX();
						int enY=enemies.elementAt(i).getY();
						if(Math.sqrt((enX-x)*(enX-x)+(enY-y)*(enY-y))<60){
							isDead=true;
						}
					}
					//计算好新的坐标后，画图，画完再睡觉
					repaint();
					if(isDead){
						while(true){
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							if(isRestart)
								break;
						}
					}
					try {
						Thread.sleep(2);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					countTime++;//用于控制小球的移动速度
				}
			}
		}.start();
	}
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_SPACE)
			isFire=true;
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			if(x>0)
				x-=35;
			break;
		case KeyEvent.VK_RIGHT:
			if(x<this.getWidth())
				x+=35;
			break;
		case KeyEvent.VK_UP:
			if(y>0)
				y-=35;
			break;
		case KeyEvent.VK_DOWN:
			if(x<this.getHeight())
				y+=35;
			break;
	    case KeyEvent.VK_SPACE://如果按一下空格键
			isFire=true;
			break;
		default:
		}
		repaint();
	}
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_SPACE:
			isFire=false;
		break;
		}
	}
	public void keyTyped(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_0)
			System.out.println("0-->"+e.getKeyCode());
		if(e.getKeyCode()==KeyEvent.VK_1)
			System.out.println("1-->"+e.getKeyCode());
		if(e.getKeyCode()==KeyEvent.VK_A)
			System.out.println("A-->"+e.getKeyCode());
		if(e.getKeyCode()==KeyEvent.VK_B)
			System.out.println("B-->"+e.getKeyCode());
	}
}
				
					
					
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
