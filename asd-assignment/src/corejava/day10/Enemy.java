package corejava.day10;

import javax.swing.*;
import java.awt.*;

public class Enemy {
    public final static int LEFT_DOWN_1 = 0;
    public final static int LEFT_DOWN_2 = 1;
    public final static int LEFT_DOWN_3 = 2;
    public final static int RIGHT_DOWN_1 = 3;
    public final static int RIGHT_DOWN_2 = 4;
    public final static int RIGHT_DOWN_3 = 5;
    public final static int DOWN = 6;

    private int x, y;//敌机中心坐标
    private int orientation;

    private JPanel panel;
    private boolean isBombed;
    private static Image enemyPlane=new ImageIcon("pic/enemy.gif").getImage();
    private static Image enemyPlane_L=new ImageIcon("pic/enemy_l.gif").getImage();
    private static Image enemyPlane_R=new ImageIcon("pic/enemy_r.gif").getImage();
    private static Image enemyBomb_1=new ImageIcon("pic/enemyBomb_1.gif").getImage();
    private static Image enemyBomb_2=new ImageIcon("pic/enemyBomb_2.gif").getImage();
    private static Image enemyBomb_3=new ImageIcon("pic/enemyBomb_3.gif").getImage();
    private static Image enemyBomb_4=new ImageIcon("pic/enemyBomb_4.gif").getImage();
    public static Image[] bomb=new Image[]{enemyBomb_1,enemyBomb_2,enemyBomb_3,enemyBomb_4};

    public Enemy(int x, int y, int orientation, JPanel panel,boolean isBombed) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.panel = panel;
        this.isBombed = isBombed;

    }
    public Enemy(){}
    public void draw(Graphics g) {
        Image tempEnemy;
        if(orientation<3)
            tempEnemy=enemyPlane_L;
        else if(3<=orientation&&orientation<6)
            tempEnemy=enemyPlane_R;
        else
            tempEnemy=enemyPlane;
        g.drawImage(tempEnemy, x-40, y-40, 80,80, panel);
    }

    public void drawEnemyBomb(Graphics g){
        if(this.getIsBombed()==true){
            g.drawImage(bomb[0],x,y,80,80, panel);
            //g.drawImage(bomb[1],x,y,80,80, panel);
            //g.drawImage(bomb[2],x,y,80,80, panel);
            //g.drawImage(bomb[3],x,y,80,80, panel);
        }
    }

    public int getX() {return x;}
    public void setX(int x) {this.x = x;}
    public int getY() {return y;}
    public void setY(int y) {this.y = y;}
    public int getOrientation() {return orientation;}
    public void setOrientation(int orientation) {this.orientation = orientation;}
    public JPanel getPanel() {return panel;}
    public void setPanel(JPanel panel) {this.panel = panel;}
    public boolean getIsBombed() {return isBombed;}
    public void setIsBombed(boolean isBombed) {this.isBombed = isBombed;}
    public void move(){
        switch (orientation) {
            case LEFT_DOWN_1:
                x--;y+=3;break;
            case LEFT_DOWN_2:
                x--;y+=2;break;
            case LEFT_DOWN_3:
                x--;y+=1;break;
            case RIGHT_DOWN_1:
                x++;y+=3;break;
            case RIGHT_DOWN_2:
                x++;y+=2;break;
            case RIGHT_DOWN_3:
                x++;y+=1;break;
            case DOWN:
                y++;break;
        }
    }
}
