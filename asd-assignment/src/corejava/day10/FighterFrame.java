package corejava.day10;
import javax.swing.JFrame;

public class FighterFrame {
	private JFrame frame;
	public FighterFrame(){
		frame=new JFrame("Mario");
		panel=new FighterPanel();//
		frame.add(panel);
		panel.startRun();
		frame.addKeyListener(panel);
	}
	public void show(){
		frame.setBounds(300, 10, 600,600);
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	public static void main(String[] args) {
		new FighterFrame().show();
	}
}